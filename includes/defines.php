<?php

/*
 * fit-to-rk
 * Copyright (C) 2021 Stéphane BARON
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Offset between Garmin (FIT) time and Unix time (Dec 31, 1989 - 00:00:00 January 1, 1970).
define('TS_OFFSET', 631065600);

define('FIT_BIG_ENDIAN', 1);
define('FIT_LITTLE_ENDIAN', 0);
define('FIT_UINT8_INVALID', 255);
define('FIT_UINT16_INVALID', 65535);
define('FIT_UINT32_INVALID', 4294967295);

define('BRYTON_MANUFACTURER_ID', 267);
define('UNKNOWN_MANUFACTURER', 'unknown');

define('RUNKEEPER_DEFAULT_SPORT', 'Other');

define('MIN_SECOND_PAUSE_DETECTION', 10);
