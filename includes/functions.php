<?php

/*
 * fit-to-rk
 * Copyright (C) 2021 Stéphane BARON
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @param $fieldBaseTypeNumber
 * @param $rawValue
 * @param $endianType
 * @return string
 */
function fitSmartUnpack($fieldBaseTypeNumber, $rawValue, $endianType)
{
    $unpackType = Fit\Helper::$packType[$endianType][$fieldBaseTypeNumber];

    if ($unpackType) {
        list(, $fieldValue) = unpack($unpackType, $rawValue);
    } elseif ($unpackType == '') {
        return strval($rawValue);
    } else {
        // Byte
        return bin2hex($rawValue);
    }

    // uint to sint
    if ($fieldBaseTypeNumber == 3) { // sint16 -> Ss
        // uint16 -> sint16
        list(, $fieldValue) = unpack('s', pack('S', $fieldValue));
    } elseif ($fieldBaseTypeNumber == 5) { // sint32 -> Ll
        // uint32 -> sint32
        list(, $fieldValue) = unpack('l', pack('L', $fieldValue));
    }

    return $fieldValue;
}

/**
 * @param $ts
 * @return false|string
 */
function getZuluTime($ts)
{
    return gmdate("Y-m-d\TH:i:s\Z", $ts);
}

/**
 * @param $ts
 * @return false|string
 */
function getRunkeeperTime($ts)
{
    // Thu, 21 Dec 2000 16:01:07
    return gmdate('D, d M Y H:i:s', $ts);
}

/**
 * @param $s
 * @return float
 */
function semicircleToDegrees($s)
{
    // d = s * (180.0 / 2^31)
    return round($s * (180 / 2147483648), 6);
}

/**
 * @param $recordFields
 * @param $ts
 */
function recordMergeData($recordFields, $ts)
{
    global $output;

    if (!isset($output[$ts])) {
        $output[$ts] = array('ts' => $ts + TS_OFFSET);
    }

    if (isset($recordFields[0])) {
        // position_lat
        $output[$ts]['lat'] = semicircleToDegrees($recordFields[0]);
    }
    if (isset($recordFields[1])) {
        // position_long
        $output[$ts]['lon'] = semicircleToDegrees($recordFields[1]);
    }
    if (isset($recordFields[2])) {
        // altitude
        $output[$ts]['alt'] = round($recordFields[2] / 5 - 500, 6);
    }
    if (isset($recordFields[3])) {
        // heart_rate
        $output[$ts]['bpm'] = $recordFields[3];
    }
    if (isset($recordFields[4]) && $recordFields[4] != FIT_UINT8_INVALID) {
        // cadence
        $output[$ts]['rpm'] = $recordFields[4];
    }
    if (isset($recordFields[5])) {
        // distance
        $output[$ts]['dst'] = ($recordFields[5] / 100);
    }
    if (isset($recordFields[6])) {
        // speed
        $output[$ts]['spd'] = ($recordFields[6] / 1000);
    }
    if (isset($recordFields[13])) {
        // temperature
        $output[$ts]['tmp'] = round($recordFields[13], 1);
    }
}

/**
 * @param $crc
 * @param $byte
 * @return int
 */
function fitCrc16Update($crc, $byte)
{
    // compute checksum of lower four bits of byte
    $tmp = \Fit\Helper::$crcTable[$crc & 0xF];
    $crc = ($crc >> 4) & 0x0FFF;
    $crc = $crc ^ $tmp ^ \Fit\Helper::$crcTable[ord($byte) & 0xF];

    // now compute checksum of upper four bits of byte
    $tmp = \Fit\Helper::$crcTable[$crc & 0xF];
    $crc = ($crc >> 4) & 0x0FFF;
    $crc = $crc ^ $tmp ^ \Fit\Helper::$crcTable[(ord($byte) >> 4) & 0xF];

    return $crc;
}

function getManufacturerName($manufacturerId)
{
    if (isset(\Fit\Helper::$antManufacturer[$manufacturerId])) {
        return \Fit\Helper::$antManufacturer[$manufacturerId];
    }
    return UNKNOWN_MANUFACTURER;
}

function getCloseTs($ts, $tsList, $threshold = 5)
{
    foreach ($tsList as $t) {
        if (abs($t - $ts) < $threshold) {
            return true;
        }
    }
    return false;
}

/**
 * @param $trackSegments
 * @param $context
 * @return mixed
 */
function createFitXml($trackSegments, $context)
{
    $xml = new DOMDocument('1.0', 'UTF-8');
    $xml->formatOutput = true;

    // Document
    $gpx = $xml->createElementNS('http://www.topografix.com/GPX/1/1', 'gpx');

    // xmlns
    $gpx->setAttributeNS(
        'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:schemaLocation',
        join(' ', array(
            'http://www.topografix.com/GPX/1/1',
            'http://www.topografix.com/GPX/1/1/gpx.xsd',
            'http://www.garmin.com/xmlschemas/TrackPointExtension/v1',
            'http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd'
        ))
    );
    $gpx->setAttributeNS(
        'http://www.w3.org/2000/xmlns/',
        'xmlns:gpxtpx',
        'http://www.garmin.com/xmlschemas/TrackPointExtension/v1'
    );

    $gpx->setAttribute('version', '1.1');
    $gpx->setAttribute('creator', 'fit-to-rk');

    // metadata
    $metadata = $xml->createElement('metadata');
    $metadata->appendChild(
        $xml->createElement('name', basename($context['source']))
    );
    $metadata->appendChild(
        $xml->createElement('time', getZuluTime($context['sessionStartTs']))
    );
    $gpx->appendChild($metadata);

    // track
    $track = $xml->createElement('trk');

    // segments
    foreach ($trackSegments as $s) {
        // empty segment
        if (!count($s)) {
            continue;
        }

        $segment = $xml->createElement('trkseg');
        foreach ($s as $p) {
            $point = $xml->createElement('trkpt');
            $point->setAttribute('lat', $p['lat']);
            $point->setAttribute('lon', $p['lon']);

            if (isset($p['alt'])) {
                $point->appendChild($xml->createElement('ele', $p['alt']));
            }

            $point->appendChild($xml->createElement('time', getZuluTime($p['ts'])));

            // TrackPointExtension
            $extensions = $xml->createElement('extensions');
            $trackPointExt = $extensions->appendChild($xml->createElement('gpxtpx:TrackPointExtension'));

            if (isset($p['tmp']) && $p['tmp'] != FIT_UINT8_INVALID) {
                $trackPointExt->appendChild($xml->createElement('gpxtpx:atemp', $p['tmp']));
            }

            if (isset($p['bpm']) && $p['bpm'] != FIT_UINT8_INVALID) {
                $trackPointExt->appendChild($xml->createElement('gpxtpx:hr', $p['bpm']));
            }

            if (isset($p['rpm']) && $p['rpm'] != FIT_UINT8_INVALID) {
                $trackPointExt->appendChild($xml->createElement('gpxtpx:cad', $p['rpm']));
            }

            $point->appendChild($extensions);

            $segment->appendChild($point);
        }

        $track->appendChild($segment);
    }

    $xml->appendChild($gpx)->appendChild($track);

    return $xml->saveXML();
}

/**
 * @param $trackSegments
 * @param $heartRates
 * @param $context
 * @return string
 */
function createFitJson($trackSegments, $heartRates, $context)
{
    // Find sport
    $rkSport = RUNKEEPER_DEFAULT_SPORT;
    if (isset(Fit\Helper::$rkType[$context['session']['sport']])) {
        $rkSport = Fit\Helper::$rkType[$context['session']['sport']];
    }

    $newFitnessActivity = array(
        'type' => $rkSport,
        'secondary_type' => '',
        'equipment' => 'None',
        'start_time' => getRunkeeperTime($context['sessionStartTs'] + $context['localTimeOffset']),
        'total_distance' => $context['session']['total_distance'],
        'duration' => $context['session']['total_timer_time'],
        'notes' => '',
        'path' => array()
    );

    $currentPoint = 0;
    foreach ($trackSegments as $s) {
        $firstPoint = true;
        foreach ($s as $p) {
            // Path
            $newFitnessActivity['path'][$currentPoint] = array(
                'timestamp' => $p['ts'] - $context['sessionStartTs'],
                'latitude' => $p['lat'],
                'longitude' => $p['lon'],
                'altitude' => isset($p['alt']) ? $p['alt'] : 0,
                'type' => 'gps' // start, end, gps, pause, resume, manual
            );

            if ($firstPoint) {
                $newFitnessActivity['path'][$currentPoint]['type'] = 'resume';
                if ($currentPoint - 1 > 0) {
                    $newFitnessActivity['path'][$currentPoint - 1]['type'] = 'pause';
                }
                $firstPoint = false;
            }

            $currentPoint++;
        }
    }

    // Heartrate
    if (count($heartRates)) {
        $newFitnessActivity['average_heart_rate'] = $context['session']['avg_bpm'];
        $newFitnessActivity['heart_rate'] = array();

        foreach ($heartRates as $ts => $hr) {
            $newFitnessActivity['heart_rate'][] = array(
                'timestamp' => $ts - $context['sessionStartTs'],
                'heart_rate' => $hr
            );
        }
    }

    // Set start and stop
    $newFitnessActivity['path'][0]['type'] = 'start';
    $newFitnessActivity['path'][$currentPoint - 1]['type'] = 'end';

    // Notes
    $rkNotes = array();
    if (isset($context['session']['avg_temperature']) && $context['session']['max_temperature']) {
        $rkNotes[] = sprintf(
            'Temp: avg/max=%s/%s C',
            $context['session']['avg_temperature'],
            $context['session']['max_temperature']
        );
    }
    if (isset($context['session']['max_speed'])) {
        $rkNotes[] = sprintf(
            'Speed: max=%s km/h',
            round($context['session']['max_speed'] * 3600 / 1000, 2)
        );
    }

    if (count($rkNotes)) {
        $newFitnessActivity['notes'] = join(', ', $rkNotes);
    }

    return json_encode($newFitnessActivity);
}
