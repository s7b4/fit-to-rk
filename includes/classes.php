<?php

/*
 * fit-to-rk
 * Copyright (C) 2021 Stéphane BARON
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Fit;

class Helper
{
    public static $packType = array(
        FIT_LITTLE_ENDIAN => array(
            0 => 'C', // enum
            1 => 'c', // sint8
            2 => 'C', // uint8
            3 => 'v', // sint16
            4 => 'v', // uint16
            5 => 'V', // sint32
            6 => 'V', // uint32
            7 => '', // string
            8 => 'f', // float32
            9 => 'd', // float64
            10 => 'C', // uint8z
            11 => 'v', // uint16z
            12 => 'V', // uint32z
            13 => null // byte
        ),
        FIT_BIG_ENDIAN => array(
            0 => 'C', // enum
            1 => 'c', // sint8
            2 => 'C', // uint8
            3 => 'n', // sint16
            4 => 'n', // uint16
            5 => 'N', // sint32
            6 => 'N', // uint32
            7 => '', // string
            8 => 'f', // float32
            9 => 'd', // float64
            10 => 'C', // uint8z
            11 => 'n', // uint16z
            12 => 'N', // uint32z
            13 => null // byte
        )
    );

    public static $invalidValues = array(
        0 => 0xFF, // enum
        1 => 0x7F, // sint8
        2 => 0xFF, // uint8
        3 => 0x7FF, // sint16
        4 => 0xFFFF, // uint16
        5 => 0x7FFFFFFF, // sint32
        6 => 0xFFFFFFFF, // uint32
        7 => 0x00, // string
        8 => 0xFFFFFFFF, // float32
        9 => 0xFFFFFFFFFFFFFFFF, // float64
        10 => 0x00, // uint8z
        11 => 0x0000, // uint16z
        12 => 0x00000000, // uint32z
        13 => 0xFF // byte
    );

    public static $rkType = array(
        1 => 'Running',
        2 => 'Cycling',
        11 => 'Walking'
    );

    public static $crcTable = array(
        0x0000,
        0xCC01,
        0xD801,
        0x1400,
        0xF001,
        0x3C00,
        0x2800,
        0xE401,
        0xA001,
        0x6C00,
        0x7800,
        0xB401,
        0x5000,
        0x9C01,
        0x8801,
        0x4400
    );

    public static $antManufacturer = array(
        1 => 'garmin',
        2 => 'garmin_fr405_antfs',
        3 => 'zephyr',
        4 => 'dayton',
        5 => 'idt',
        6 => 'srm',
        7 => 'quarq',
        8 => 'ibike',
        9 => 'saris',
        10 => 'spark_hk',
        11 => 'tanita',
        12 => 'echowell',
        13 => 'dynastream_oem',
        14 => 'nautilus',
        15 => 'dynastream',
        16 => 'timex',
        17 => 'metrigear',
        18 => 'xelic',
        19 => 'beurer',
        20 => 'cardiosport',
        21 => 'a_and_d',
        22 => 'hmm',
        23 => 'suunto',
        24 => 'thita_elektronik',
        25 => 'gpulse',
        26 => 'clean_mobile',
        27 => 'pedal_brain',
        28 => 'peaksware',
        29 => 'saxonar',
        30 => 'lemond_fitness',
        31 => 'dexcom',
        32 => 'wahoo_fitness',
        33 => 'octane_fitness',
        34 => 'archinoetics',
        35 => 'the_hurt_box',
        36 => 'citizen_systems',
        37 => 'magellan',
        38 => 'osynce',
        39 => 'holux',
        40 => 'concept2',
        42 => 'one_giant_leap',
        43 => 'ace_sensor',
        44 => 'brim_brothers',
        45 => 'xplova',
        46 => 'perception_digital',
        47 => 'bf1systems',
        48 => 'pioneer',
        49 => 'spantec',
        50 => 'metalogics',
        51 => '4iiiis',
        52 => 'seiko_epson',
        53 => 'seiko_epson_oem',
        54 => 'ifor_powell',
        55 => 'maxwell_guider',
        56 => 'star_trac',
        57 => 'breakaway',
        58 => 'alatech_technology_ltd',
        59 => 'mio_technology_europe',
        60 => 'rotor',
        61 => 'geonaute',
        62 => 'id_bike',
        63 => 'specialized',
        64 => 'wtek',
        65 => 'physical_enterprises',
        66 => 'north_pole_engineering',
        67 => 'bkool',
        68 => 'cateye',
        69 => 'stages_cycling',
        70 => 'sigmasport',
        71 => 'tomtom',
        72 => 'peripedal',
        73 => 'wattbike',
        76 => 'moxy',
        77 => 'ciclosport',
        78 => 'powerbahn',
        79 => 'acorn_projects_aps',
        80 => 'lifebeam',
        81 => 'bontrager',
        82 => 'wellgo',
        83 => 'scosche',
        84 => 'magura',
        85 => 'woodway',
        86 => 'elite',
        87 => 'nielsen_kellerman',
        88 => 'dk_city',
        89 => 'tacx',
        90 => 'direction_technology',
        91 => 'magtonic',
        92 => '1partcarbon',
        93 => 'inside_ride_technologies',
        94 => 'sound_of_motion',
        95 => 'stryd',
        96 => 'icg',
        97 => 'MiPulse',
        98 => 'bsx_athletics',
        99 => 'look',
        100 => 'campagnolo_srl',
        101 => 'body_bike_smart',
        102 => 'praxisworks',
        103 => 'limits_technology',
        104 => 'topaction_technology',
        105 => 'cosinuss',
        106 => 'fitcare',
        107 => 'magene',
        108 => 'giant_manufacturing_co',
        109 => 'tigrasport',
        110 => 'salutron',
        111 => 'technogym',
        112 => 'bryton_sensors',
        113 => 'latitude_limited',
        114 => 'soaring_technology',
        115 => 'igpsport',
        116 => 'thinkrider',
        117 => 'gopher_sport',
        118 => 'waterrower',
        119 => 'orangetheory',
        120 => 'inpeak',
        121 => 'kinetic',
        122 => 'johnson_health_tech',
        123 => 'polar_electro',
        124 => 'seesense',
        125 => 'nci_technology',
        126 => 'iqsquare',
        127 => 'leomo',
        128 => 'ifit_com',
        129 => 'coros_byte',
        130 => 'versa_design',
        131 => 'chileaf',
        132 => 'cycplus',
        133 => 'gravaa_byte',
        255 => 'development',
        257 => 'healthandlife',
        258 => 'lezyne',
        259 => 'scribe_labs',
        260 => 'zwift',
        261 => 'watteam',
        262 => 'recon',
        263 => 'favero_electronics',
        264 => 'dynovelo',
        265 => 'strava',
        266 => 'precor',
        267 => 'bryton',
        268 => 'sram',
        269 => 'navman',
        270 => 'cobi',
        271 => 'spivi',
        272 => 'mio_magellan',
        273 => 'evesports',
        274 => 'sensitivus_gauge',
        275 => 'podoon',
        276 => 'life_time_fitness',
        277 => 'falco_e_motors',
        278 => 'minoura',
        279 => 'cycliq',
        280 => 'luxottica',
        281 => 'trainer_road',
        282 => 'the_sufferfest',
        283 => 'fullspeedahead',
        284 => 'virtualtraining',
        285 => 'feedbacksports',
        286 => 'omata',
        287 => 'vdo',
        288 => 'magneticdays',
        289 => 'hammerhead',
        290 => 'kinetic_by_kurt',
        291 => 'shapelog',
        292 => 'dabuziduo',
        293 => 'jetblack',
        294 => 'coros',
        295 => 'virtugo',
        296 => 'velosense',
        297 => 'cycligentinc',
        298 => 'trailforks',
        299 => 'mahle_ebikemotion',
        300 => 'nurvv',
        301 => 'microprogram',
        302 => 'zone5cloud',
        303 => 'greenteg',
        304 => 'yamaha_motors',
        305 => 'whoop',
        306 => 'gravaa',
        307 => 'onelap',
        308 => 'monark_exercise',
        309 => 'form',
        5759 => 'actigraphcorp'
    );
}
