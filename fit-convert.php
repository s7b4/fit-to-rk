<?php

/*
 * fit-to-rk
 * Copyright (C) 2021 Stéphane BARON
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$appRoot = __DIR__;

require $appRoot . '/vendor/autoload.php';
require $appRoot . '/includes/defines.php';
require $appRoot . '/includes/classes.php';
require $appRoot . '/includes/functions.php';

use Monolog\Logger;
use Monolog\Handler\ErrorLogHandler;

date_default_timezone_set('UTC');
ini_set('error_reporting', 0);

if (PHP_SAPI !== 'cli') {
    echo 'Warning: fit-convert should be invoked via the CLI version of PHP, not the ' . PHP_SAPI . ' SAPI' . PHP_EOL;
    exit(1);
}

/**
 * Parsing des options
 */
$parser = Console_CommandLine::fromXmlFile($appRoot . '/fit-convert.xml');
try {
    $execOptions = $parser->parse();
} catch (Exception $exc) {
    $parser->displayError($exc->getMessage(), false);
    exit(1);
}

$logger = new Logger('fit-convert');
$logger->pushHandler(
    new ErrorLogHandler(
        ErrorLogHandler::OPERATING_SYSTEM,
        $execOptions->options['verbose'] ? Logger::INFO : Logger::WARNING
    )
);

// Input file
$inputFile = $execOptions->args['input'];
if (!$inputFile || !is_file($inputFile)) {
    $logger->addError('Unable to read input file', array('intputFile' => $inputFile));
    exit(2);
}

// Output file
$outputFile = $execOptions->args['output'];
if (is_null($outputFile)) {
    $outputFile = 'php://output';
}

// Mode
$outputMode = $execOptions->options['mode'];

// Force write
$forceWrite = $execOptions->options['overwrite'];
if (is_file($outputFile) && $forceWrite !== true) {
    $logger->addError('Output file already exists', array('outputFile' => $outputFile));
    exit(2);
}

/**************************
 * Init context
 **************************/
$output = array();
$splittedOutput = array();
$heartOutput = array();
$rpmOutput = array();
$segmentStart = array();

$context = array(
    'sessionStartTs' => 0,
    'sessionEndTs' => time(),
    'localTimeOffset' => 0,
    'headers' => null,
    'activity' => null,
    'session' => null,
    'file' => null,
    'source' => realpath($inputFile)
);

$currentTimestamp = TS_OFFSET;

$h = fopen($inputFile, 'rb');
$fileSize = fstat($h)['size'];

// Calculate file CRC
fseek($h, 0);
$computedFileHeaders = null;
for ($i = 0; $i < $fileSize - 2; $i++) {
    $computedFileHeaders = fitCrc16Update($computedFileHeaders, fread($h, 1));
}
$logger->addInfo('CRC of file', array('crc' => sprintf('0x%s', bin2hex($computedFileHeaders))));

// Calculate headers CRC
fseek($h, 0);
$computedCrcHeaders = null;
for ($i = 0; $i < 12; $i++) {
    $computedCrcHeaders = fitCrc16Update($computedCrcHeaders, fread($h, 1));
}
$logger->addInfo('CRC of headers', array('crc' => sprintf('0x%s', bin2hex($computedCrcHeaders))));

$context['headers']['fileSize'] = $fileSize;

// Read Header size
fseek($h, 0);
$readBuffer = fread($h, 1);
list(, $context['headers']['headerSize']) = unpack('C', $readBuffer);

$readBuffer = fread($h, 1);
list(, $context['headers']['protocolVersion']) = unpack('C', $readBuffer);

// 1010 => 10.10
$readBuffer = fread($h, 2);
list(, $context['headers']['profileVersion']) = unpack('v', $readBuffer);

$readBuffer = fread($h, 4);
list(, $context['headers']['dataSize']) = unpack('V', $readBuffer);

$context['headers']['dataType'] = fread($h, 4);

$logger->addInfo('Fit headers', $context['headers']);

// Datatype check
if ($context['headers']['dataType'] != '.FIT') {
    $logger->addError('Bad dataType', array('actual' => $context['headers']['dataType'], 'expected' => '.FIT'));
    exit(2);
}

// Headers CRC check
if ($context['headers']['headerSize'] > 12) {
    // Contains the value of the CRC of Bytes 0 through 11, or may be set to 0x0000. This field is optional.
    $readBuffer = fread($h, 2);
    list(, $crcHeaders) = unpack('v', $readBuffer);
    $logger->addInfo(
        'CRC of headers',
        array(
            'expected' => sprintf('0x%s', bin2hex($crcHeaders)),
            'actual' => sprintf('0x%s', bin2hex($computedCrcHeaders)),

        )
    );

    if (!$crcHeaders) {
        $logger->addInfo('CRC of headers not provided');
    } elseif ($crcHeaders !== $computedCrcHeaders) {
        $logger->addError('Corrupted headers, bad CRC');
        exit(2);
    }
} else {
    $logger->addInfo('No optional CRC of headers');
}

// Size check
if ($context['headers']['fileSize'] != ($context['headers']['dataSize'] + $context['headers']['headerSize'] + 2)) {
    $logger->addError(
        'Bad file size',
        array(
            'actual' => $context['headers']['fileSize'],
            'expected' => $context['headers']['dataSize'] + $context['headers']['headerSize'] + 2
        )
    );
    exit(2);
}

$definitionDataTable = array();
$dataSizeWithHeaders = $context['headers']['dataSize'] + $context['headers']['headerSize'];

while (($readBufferPosition = ftell($h)) < $dataSizeWithHeaders) {
    // Message
    $readBuffer = fread($h, 1);
    list(, $recordHeader) = unpack('C', $readBuffer);

    // Record
    $normalHeaderFlag = ($recordHeader >> 7) & 0x1;

    if (!$normalHeaderFlag) {
        $messageType = ($recordHeader >> 6) & 0x1; // 0:Definition 1:Data
        $messageTypeSpecific = ($recordHeader >> 5) & 0x1;
        $recordHeaderReserved = ($recordHeader >> 4) & 0x1; // 0
        $localMessageType = $recordHeader & 0xF;

        if ($messageType == 1) {
            // Definition message
            $readBuffer = fread($h, 1);
            list(, $recordContentReserved) = unpack('C', $readBuffer);

            $readBuffer = fread($h, 1);
            list(, $recordContentArchitecture) = unpack('C', $readBuffer); // 0: little endian 1: Big endian

            $readBuffer = fread($h, 2);
            list(, $recordContentGlobalMessageNumber) = unpack(($recordContentArchitecture) ? 'n' : 'v', $readBuffer);

            $readBuffer = fread($h, 1);
            list(, $recordContentNumberOfFields) = unpack('C', $readBuffer);

            $logger->addDebug(
                'Definition message',
                array(
                    'localMessageType' => $localMessageType,
                    'Fields' => $recordContentNumberOfFields,
                    'globalMessageNumber' => $recordContentGlobalMessageNumber,
                    'endian' => $recordContentArchitecture
                )
            );

            $fieldList = array();
            $dataMessageSize = array();

            for ($fieldNumber = 0; $fieldNumber < $recordContentNumberOfFields; $fieldNumber++) {
                $readBuffer = fread($h, 1);
                list(, $fieldDefinitionNumber) = unpack('C', $readBuffer);

                $readBuffer = fread($h, 1);
                list(, $fieldSize) = unpack('C', $readBuffer);

                $readBuffer = fread($h, 1);
                list(, $fieldBaseType) = unpack('C', $readBuffer);

                $fieldBaseTypeEndian = ($fieldBaseType >> 7) & 0x1;
                $fieldBaseTypeReserved = ($fieldBaseType >> 4) & 0x3;
                $fieldBaseTypeNumber = $fieldBaseType & 0x1F;

                $fieldList[] = array(
                    'fieldDefinitionNumber' => $fieldDefinitionNumber,
                    'fieldSize' => $fieldSize,
                    'fieldBaseType' => $fieldBaseType,
                    'fieldBaseEndian' => $fieldBaseTypeEndian,
                    'fieldBaseTypeReserved' => $fieldBaseTypeReserved,
                    'fieldBaseTypeNumber' => $fieldBaseTypeNumber
                );

                $dataMessageSize[] = $fieldSize;
            }

            $devFieldList = array();
            $devDataMessageSize = array();
            if ($messageTypeSpecific) {
                $readBuffer = fread($h, 1);
                $recordContentNumberOfDevFields = unpack('C', $readBuffer)[1];

                for ($fieldNumber = 0; $fieldNumber < $recordContentNumberOfFields; $fieldNumber++) {
                    $readBuffer = fread($h, 1);
                    list(, $fieldDefinitionNumber) = unpack('C', $readBuffer); // 255: Invalid

                    $readBuffer = fread($h, 1);
                    list(, $fieldSize) = unpack('C', $readBuffer);

                    $readBuffer = fread($h, 1);
                    list(, $devDataIndex) = unpack('C', $readBuffer);

                    $devDataMessageSize[] = $fieldSize;
                }
            }

            $definitionDataTable[$localMessageType] = array(
                'recordContentReserved' => $recordContentReserved,
                'recordContentArchitecture' => $recordContentArchitecture,// 0:Little 1:Big
                'recordContentGlobalMessageNumber' => $recordContentGlobalMessageNumber,
                'recordContentNumberOfFields' => $recordContentNumberOfFields,
                'messageFields' => $fieldList,
                'devMessageFields' => $devFieldList,
                'messageSize' => array_sum($dataMessageSize) + array_sum($devDataMessageSize)
            );
        } else {
            $currentDefinition = $definitionDataTable[$localMessageType];
            $currentMessageSize = $currentDefinition['messageSize'];
            $currentGlobalMessageNumber = $currentDefinition['recordContentGlobalMessageNumber'];

            $logger->addDebug('Data message', array(
                'localMessageType' => $localMessageType,
                'Size' => $currentMessageSize,
                'globalMessageNumber' => $currentGlobalMessageNumber
            ));

            $currentFields = array();
            foreach ($currentDefinition['messageFields'] as $f) {
                $currentFields[$f['fieldDefinitionNumber']] = fitSmartUnpack(
                    $f['fieldBaseTypeNumber'],
                    fread($h, $f['fieldSize']),
                    $currentDefinition['recordContentArchitecture']
                );
            }

            if (isset($currentFields[253])) {
                // Track timestamp
                $currentTimestamp = $currentFields[253];
            }

            if ($currentGlobalMessageNumber == 0) { // First Message
                // file_id
                $context['file'] = array(
                    'type' => $currentFields[0],
                    'manufacturer' => $currentFields[1],
                    'product' => $currentFields[2],
                    'serial_number' => $currentFields[3],
                    'time_created' => $currentFields[4] + TS_OFFSET,
                    'number' => isset($currentFields[5]) ? $currentFields[5] : null,
                    'product_name' => isset($currentFields[8]) ? $currentFields[8] : null
                );

                $logger->addInfo('FileID', $context['file']);

                if ($context['file']['manufacturer'] != BRYTON_MANUFACTURER_ID) {
                    $logger->addWarning(
                        'This is not a bryton file',
                        array(
                            'manufacturer' => sprintf(
                                '%s (%d)',
                                getManufacturerName($context['file']['manufacturer']),
                                $context['file']['manufacturer']
                            )
                        )
                    );
                }
            } elseif ($currentGlobalMessageNumber == 18) {
                // session
                $context['session'] = array(
                    'ts' => $currentTimestamp + TS_OFFSET,
                    'event' => $currentFields[0],
                    'event_type' => $currentFields[1],
                    'start_time' => $currentFields[2] + TS_OFFSET,
                    'start_position_lat' => semicircleToDegrees($currentFields[3]),
                    'start_position_long' => semicircleToDegrees($currentFields[4]),
                    'sport' => $currentFields[5],
                    'sub_sport' => $currentFields[6],
                    'total_elapsed_time' => $currentFields[7] / 1000,
                    'total_timer_time' => $currentFields[8] / 1000,
                    'total_distance' => $currentFields[9] / 100,
                    'avg_speed' => isset($currentFields[14]) && $currentFields[14] != FIT_UINT16_INVALID ?
                        $currentFields[14] / 1000 :
                        null,
                    'max_speed' => isset($currentFields[15]) && $currentFields[15] != FIT_UINT16_INVALID ?
                        $currentFields[15] / 1000 :
                        null,
                    'avg_bpm' => isset($currentFields[16]) && $currentFields[16] != FIT_UINT8_INVALID ?
                        $currentFields[16] :
                        null,
                    'max_bpm' => isset($currentFields[17]) && $currentFields[17] != FIT_UINT8_INVALID ?
                        $currentFields[17] :
                        null,
                    'avg_rpm' => isset($currentFields[18]) && $currentFields[18] != FIT_UINT8_INVALID ?
                        $currentFields[18] :
                        null,
                    'max_rpm' => isset($currentFields[19]) && $currentFields[19] != FIT_UINT8_INVALID ?
                        $currentFields[19] :
                        null,
                    'total_ascent' => $currentFields[22],
                    'total_descent' => $currentFields[23],
                    'avg_altitude' => isset($currentFields[49]) ? $currentFields[49] / 5 - 500 : null,
                    'max_altitude' => isset($currentFields[50]) ? $currentFields[50] / 5 - 500 : null,
                    'avg_temperature' => isset($currentFields[57]) ? $currentFields[57] : null,
                    'max_temperature' => isset($currentFields[58]) ? $currentFields[58] : null,
                    'total_moving_time' => isset($currentFields[59]) ? $currentFields[59] / 1000 : null,
                );
                $logger->addInfo('Session', $context['session']);

                $context['sessionStartTs'] = $context['session']['start_time'];
                $context['sessionEndTs'] = $context['sessionStartTs'] + $context['session']['total_elapsed_time'];
            } elseif ($currentGlobalMessageNumber == 20) {
                // record
                recordMergeData($currentFields, $currentTimestamp);
            } elseif ($currentGlobalMessageNumber == 19) {
                // lap
            } elseif ($currentGlobalMessageNumber == 12) {
                // sport
            } elseif ($currentGlobalMessageNumber == 21) {
                // event
                if ($currentFields[0] == 0) {
                    if ($currentFields[1] == 0) {
                        // Timer start
                        $segmentStart[] = $currentTimestamp + TS_OFFSET;
                        $logger->addDebug('Timer', array('start' => $currentTimestamp + TS_OFFSET));
                    } elseif ($currentFields[1] == 1) {
                        // Timer stop
                        $logger->addDebug('Timer', array('stop' => $currentTimestamp + TS_OFFSET));
                    } elseif ($currentFields[1] == 4) {
                        // Timer stop_all
                        $logger->addDebug('Timer', array('stop_all' => $currentTimestamp + TS_OFFSET));
                    }
                }
            } elseif ($currentGlobalMessageNumber == 22) {
                // source (Garmin)
            } elseif ($currentGlobalMessageNumber == 23) {
                // device_info
            } elseif ($currentGlobalMessageNumber == 34) {
                // activity
                $context['activity'] = array(
                    'ts' => $currentTimestamp + TS_OFFSET,
                    'total_timer_time' => $currentFields[0] / 1000,
                    'num_sessions' => $currentFields[1],
                    'type' => $currentFields[2],
                    'event' => $currentFields[3],
                    'event_type' => $currentFields[4],
                    'local_timestamp' => ($currentFields[5]) ?
                        $currentFields[5] + TS_OFFSET :
                        $currentTimestamp + TS_OFFSET,
                    'event_group' => isset($currentFields[6]) ? $currentFields[6] : null
                );
                $logger->addInfo('Activity', $context['activity']);

                $context['localTimeOffset'] = $context['activity']['local_timestamp'] - $context['activity']['ts'];
                $logger->addInfo('Timing', array('localTimeOffset' => $context['localTimeOffset']));
            } elseif ($currentGlobalMessageNumber == 49) {
                // file creator
                $logger->addInfo('File creator', $currentFields);
            } elseif ($currentGlobalMessageNumber == 68) {
                // ??
            } elseif ($currentGlobalMessageNumber == 79) {
                // HR zones (Garmin)
            } elseif ($currentGlobalMessageNumber == 104) {
                // battery (Garmin)
            } elseif ($currentGlobalMessageNumber == 113) {
                // ??
            } elseif ($currentGlobalMessageNumber == 140) {
                // ??
            } elseif ($currentGlobalMessageNumber == 65282) {
                // ??
            } else {
                $logger->addWarning('Skip Message', array('globalMessageNumber' => $currentGlobalMessageNumber));
                // var_dump($currentFields);
            }
        }
    } else {
        // Compressed Data Message
        $localMessageType = ($recordHeader >> 5) & 0x3;
        $timeOffset = $recordHeader & 0x1F;

        $currentDefinition = $definitionDataTable[$localMessageType];
        $currentMessageSize = $currentDefinition['messageSize'];
        $currentGlobalMessageNumber = $currentDefinition['recordContentGlobalMessageNumber'];

        $logger->addDebug(
            'Data compressed message',
            array(
                'localMessageType' => $localMessageType,
                'Size' => $currentMessageSize,
                'globalMessageNumber' => $currentGlobalMessageNumber,
                'offset' => $timeOffset
            )
        );

        if ($timeOffset >= ($currentTimestamp & 0x1F)) {
            // Timestamp = (Previous timestamp)&0xFFFFFFE0 + Time Offset
            $compressedTimestamp = ($currentTimestamp & 0xFFFFFFE0) + $timeOffset;
        } else {
            // Timestamp = (Previous timestamp)&0xFFFFFFE0 + Time Offset + 0x20
            $compressedTimestamp = ($currentTimestamp & 0xFFFFFFE0) + $timeOffset + 0x20;
        }

        $currentFields = array();
        foreach ($currentDefinition['messageFields'] as $f) {
            $currentFields[$f['fieldDefinitionNumber']] = fitSmartUnpack(
                $f['fieldBaseTypeNumber'],
                fread($h, $f['fieldSize']),
                $currentDefinition['recordContentArchitecture']
            );
        }

        if ($currentGlobalMessageNumber == 20) {
            // record
            recordMergeData($currentFields, $compressedTimestamp);
        } elseif ($currentGlobalMessageNumber == 69) {
            // ??
        } else {
            $logger->addWarning('Skip compressed message', array('globalMessageNumber' => $currentGlobalMessageNumber));
            // var_dump($currentFields);
        }
    }
}

// Read CRC of file
$readBuffer = fread($h, 2);
list(, $crcFile) = unpack('v', $readBuffer);

$logger->addInfo(
    'CRC of file',
    array(
        'expected' => sprintf('0x%s', bin2hex($crcFile)),
        'actual' => sprintf('0x%s', bin2hex($computedFileHeaders)),

    )
);

// Check CRC / https://developer.garmin.com/fit/protocol/#crc
if ($crcFile != $computedFileHeaders) {
    $logger->addError('Corrupted file, bad CRC');
    exit(2);
}

// Vérification des points
if ($count = count($output) <= 2) {
    $logger->addError('Not enough trackpoints', array('count' => $count));
    exit(2);
}

// Découpage en segments
$currentSegment = 0;
$nextSegmentStart = $context['sessionStartTs'];
foreach ($output as $p) {
    // Segment split
    if ($p['ts'] >= $nextSegmentStart) {
        $currentSegment++;
        $splittedOutput[$currentSegment] = array();

        $nextSegmentStart = array_shift($segmentStart);

        // Fix duplicate
        if ($nextSegmentStart == $p['ts']) {
            $nextSegmentStart = array_shift($segmentStart);
        }

        // Fix last segment
        if (is_null($nextSegmentStart)) {
            $nextSegmentStart = $context['sessionEndTs'];
        }
    }

    // Heart rate
    if (isset($p['bpm']) && $p['bpm'] != FIT_UINT8_INVALID) {
        $heartOutput[$p['ts']] = $p['bpm'];
    }

    // Rpm
    if (isset($p['rpm']) && $p['rpm'] != FIT_UINT8_INVALID) {
        $rpmOutput[$p['ts']] = $p['rpm'];
    }

    // Skip bad trackpoint
    if (!isset($p['lat']) || !isset($p['lon'])) {
        continue;
    }

    // Skip pauses
    //if (isset($p['spd']) && $p['spd'] == 0) {
    //    continue;
    //}

    $splittedOutput[$currentSegment][] = $p;
}

// Ecriture du fichier
$writeBytes = file_put_contents(
    $outputFile,
    ($outputMode == 'json') ?
        createFitJson($splittedOutput, $heartOutput, $context) :
        createFitXml($splittedOutput, $context)
);

if (!$writeBytes) {
    $logger->addError('Unable to write file', array('outputFile' => $outputFile));
    exit(2);
}

$logger->addInfo(
    'Memory used',
    array(
        'cur' => round(memory_get_usage() / 1024 / 1024, 2) . 'MB',
        'max' => round(memory_get_peak_usage() / 1024 / 1024, 2) . 'MB'
    )
);
