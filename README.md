# fit-to-rk

## Synopsis

This script convert a fit activity file into a gpx or json file. Json file can be uploaded to [Runkeeper Healt Graph API](https://runkeeper.com/developer/healthgraph/)

:warning: This is only tested with Bryton Rider 310, 410 and 10 :warning:

## Usage

```
$ ./fit-convert.phar --help
Convert Bryton Fit file to GPX/JSON

Usage:
  ./fit-convert.phar [options] input [output]

Options:
  -v, --verbose         turn on verbose output
  -f, --force           overwrite output file
  -m mode, --mode=mode  Use json or gpx output
  -h, --help            show this help message and exit
  --version             show the program version and exit

Arguments:
  input   Fit file
  output  Output file
```
## Download

Download latest [fit-convert.phar](https://s3-eu-west-1.amazonaws.com/fit-to-rk/fit-convert.phar)

## License

[GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

## External links

* [FIT SDK](https://www.thisisant.com/resources/fit)
* [Bryton](http://corp.brytonsport.com/)